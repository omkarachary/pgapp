﻿
using PGApp.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;


namespace Repository
{
    public class SQLUnitOfWork : IDisposable
    {
        DbContext AppdbContext = new PgAppDb();


        private IRepository<User> _usersRepo;

        public IRepository<User> UsersRepo
        {
            get {
                if (_usersRepo == null)
                    _usersRepo = new SQLRepository<User>(AppdbContext);

                return _usersRepo; }
            set { _usersRepo = value; }
        }


        public DbContext  context { get; set; }

        

        public SQLUnitOfWork()
        {
        
        }

        public SQLUnitOfWork(DbContext context)
        {
            this.context = context;
        }




        
        public void commit()
        {
            AppdbContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose();
            GC.SuppressFinalize(this);
        }
        //public bool DeleteAllAndResetIdentity()
        //{
            
        //    DatabaseContext.Database.ExecuteSqlCommand("DELETE  FROM Enquiry");
        //    DatabaseContext.Database.ExecuteSqlCommand("DELETE  FROM Course");
        //    DatabaseContext.Database.ExecuteSqlCommand("DELETE  FROM CourseEnquiry");
        //    DatabaseContext.Database.ExecuteSqlCommand("DELETE  FROM Faculty");
        //    DatabaseContext.Database.ExecuteSqlCommand("DELETE  FROM Review");
        //    DatabaseContext.Database.ExecuteSqlCommand("DELETE  FROM Student");

        //    DatabaseContext.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Course', RESEED, 0)");
        //    DatabaseContext.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Enquiry', RESEED, 0)");
        //    DatabaseContext.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Faculty', RESEED, 0)");
        //    DatabaseContext.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Review', RESEED, 0)");
        //    DatabaseContext.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Student', RESEED, 0)");
        //    return true;

        //}
    }
}