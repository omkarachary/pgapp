﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGApp.Entity
{
  public  class User
    {

        public int ID { get; set; }

        public String Name { get; set; }

        public String  Address { get; set; }

       
        public int PhoneNo { get; set; }

        public String Email { get; set; }

        public bool IsActive { get; set; }

        public DbSet<Accomodation> MyProperty { get; set; }



    }
}
